# UP1 DEPLOYMENT CONFIGURATION
class site_up1 {

  # Set Up1
  class { 'up1':
    app_api_key    => '59Mnk5nY6eCn4bi9GvfOXhMH54E7Bh6EMJXtyJfs',
    app_delete_key => 'HcGVfVEM629ABi0syP5zWsUnFC3dWqthS0vZjUWk',
    app_email      => 'https://help.riseup.net/en/about-us/contact',
    app_http_port  => '8007'
  }


  cron { 'purge_old_up1':
    command => '/usr/bin/find /srv/up1/i -type f -mtime +7 -exec rm {} \;',
    user    => 'up1',
    hour    => 2,
    minute  => 0
  }

  include nginx
  # Set NGINX Reverse Proxy
  nginx::vhost { 'up1': }

}
  
